// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FWeaponProperties;
#ifdef TPSGAME_TpsGameInstance_generated_h
#error "TpsGameInstance.generated.h already included, missing '#pragma once' in TpsGameInstance.h"
#endif
#define TPSGAME_TpsGameInstance_generated_h

#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_SPARSE_DATA
#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWeaponByName);


#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWeaponByName);


#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTpsGameInstance(); \
	friend struct Z_Construct_UClass_UTpsGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTpsGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(UTpsGameInstance)


#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUTpsGameInstance(); \
	friend struct Z_Construct_UClass_UTpsGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTpsGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(UTpsGameInstance)


#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTpsGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTpsGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTpsGameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTpsGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTpsGameInstance(UTpsGameInstance&&); \
	NO_API UTpsGameInstance(const UTpsGameInstance&); \
public:


#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTpsGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTpsGameInstance(UTpsGameInstance&&); \
	NO_API UTpsGameInstance(const UTpsGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTpsGameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTpsGameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTpsGameInstance)


#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_PRIVATE_PROPERTY_OFFSET
#define TpsGame_Source_TpsGame_TpsGameInstance_h_14_PROLOG
#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_SPARSE_DATA \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_RPC_WRAPPERS \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_INCLASS \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TpsGame_Source_TpsGame_TpsGameInstance_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_SPARSE_DATA \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_INCLASS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_TpsGameInstance_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPSGAME_API UClass* StaticClass<class UTpsGameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TpsGame_Source_TpsGame_TpsGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
