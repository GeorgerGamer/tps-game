// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TpsGame/TpsGameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTpsGameInstance() {}
// Cross Module References
	TPSGAME_API UClass* Z_Construct_UClass_UTpsGameInstance_NoRegister();
	TPSGAME_API UClass* Z_Construct_UClass_UTpsGameInstance();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstance();
	UPackage* Z_Construct_UPackage__Script_TpsGame();
	TPSGAME_API UScriptStruct* Z_Construct_UScriptStruct_FWeaponProperties();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UTpsGameInstance::execGetWeaponByName)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_WeaponName);
		P_GET_STRUCT(FWeaponProperties,Z_Param_OutputWeapon);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetWeaponByName(Z_Param_WeaponName,Z_Param_OutputWeapon);
		P_NATIVE_END;
	}
	void UTpsGameInstance::StaticRegisterNativesUTpsGameInstance()
	{
		UClass* Class = UTpsGameInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWeaponByName", &UTpsGameInstance::execGetWeaponByName },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics
	{
		struct TpsGameInstance_eventGetWeaponByName_Parms
		{
			FName WeaponName;
			FWeaponProperties OutputWeapon;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_WeaponName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputWeapon;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_WeaponName = { "WeaponName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TpsGameInstance_eventGetWeaponByName_Parms, WeaponName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_OutputWeapon = { "OutputWeapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TpsGameInstance_eventGetWeaponByName_Parms, OutputWeapon), Z_Construct_UScriptStruct_FWeaponProperties, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TpsGameInstance_eventGetWeaponByName_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TpsGameInstance_eventGetWeaponByName_Parms), &Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_WeaponName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_OutputWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TpsGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTpsGameInstance, nullptr, "GetWeaponByName", nullptr, nullptr, sizeof(TpsGameInstance_eventGetWeaponByName_Parms), Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTpsGameInstance_NoRegister()
	{
		return UTpsGameInstance::StaticClass();
	}
	struct Z_Construct_UClass_UTpsGameInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WeaponTable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTpsGameInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTpsGameInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTpsGameInstance_GetWeaponByName, "GetWeaponByName" }, // 1260475783
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTpsGameInstance_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "TpsGameInstance.h" },
		{ "ModuleRelativePath", "TpsGameInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTpsGameInstance_Statics::NewProp_WeaponTable_MetaData[] = {
		{ "Category", "Projectile Settings" },
		{ "ModuleRelativePath", "TpsGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTpsGameInstance_Statics::NewProp_WeaponTable = { "WeaponTable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTpsGameInstance, WeaponTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTpsGameInstance_Statics::NewProp_WeaponTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTpsGameInstance_Statics::NewProp_WeaponTable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTpsGameInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTpsGameInstance_Statics::NewProp_WeaponTable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTpsGameInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTpsGameInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTpsGameInstance_Statics::ClassParams = {
		&UTpsGameInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTpsGameInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTpsGameInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTpsGameInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTpsGameInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTpsGameInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTpsGameInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTpsGameInstance, 1824766442);
	template<> TPSGAME_API UClass* StaticClass<UTpsGameInstance>()
	{
		return UTpsGameInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTpsGameInstance(Z_Construct_UClass_UTpsGameInstance, &UTpsGameInstance::StaticClass, TEXT("/Script/TpsGame"), TEXT("UTpsGameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTpsGameInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
