// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TpsGame/Game/TpsGamePlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTpsGamePlayerController() {}
// Cross Module References
	TPSGAME_API UClass* Z_Construct_UClass_ATpsGamePlayerController_NoRegister();
	TPSGAME_API UClass* Z_Construct_UClass_ATpsGamePlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_TpsGame();
// End Cross Module References
	void ATpsGamePlayerController::StaticRegisterNativesATpsGamePlayerController()
	{
	}
	UClass* Z_Construct_UClass_ATpsGamePlayerController_NoRegister()
	{
		return ATpsGamePlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ATpsGamePlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATpsGamePlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATpsGamePlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Game/TpsGamePlayerController.h" },
		{ "ModuleRelativePath", "Game/TpsGamePlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATpsGamePlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATpsGamePlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATpsGamePlayerController_Statics::ClassParams = {
		&ATpsGamePlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATpsGamePlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATpsGamePlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATpsGamePlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATpsGamePlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATpsGamePlayerController, 908888373);
	template<> TPSGAME_API UClass* StaticClass<ATpsGamePlayerController>()
	{
		return ATpsGamePlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATpsGamePlayerController(Z_Construct_UClass_ATpsGamePlayerController, &ATpsGamePlayerController::StaticClass, TEXT("/Script/TpsGame"), TEXT("ATpsGamePlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATpsGamePlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
