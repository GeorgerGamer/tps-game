// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPSGAME_TpsGamePlayerController_generated_h
#error "TpsGamePlayerController.generated.h already included, missing '#pragma once' in TpsGamePlayerController.h"
#endif
#define TPSGAME_TpsGamePlayerController_generated_h

#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_SPARSE_DATA
#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_RPC_WRAPPERS
#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATpsGamePlayerController(); \
	friend struct Z_Construct_UClass_ATpsGamePlayerController_Statics; \
public: \
	DECLARE_CLASS(ATpsGamePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(ATpsGamePlayerController)


#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATpsGamePlayerController(); \
	friend struct Z_Construct_UClass_ATpsGamePlayerController_Statics; \
public: \
	DECLARE_CLASS(ATpsGamePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(ATpsGamePlayerController)


#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATpsGamePlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATpsGamePlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATpsGamePlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATpsGamePlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATpsGamePlayerController(ATpsGamePlayerController&&); \
	NO_API ATpsGamePlayerController(const ATpsGamePlayerController&); \
public:


#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATpsGamePlayerController(ATpsGamePlayerController&&); \
	NO_API ATpsGamePlayerController(const ATpsGamePlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATpsGamePlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATpsGamePlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATpsGamePlayerController)


#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_9_PROLOG
#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_SPARSE_DATA \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_RPC_WRAPPERS \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_INCLASS \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_SPARSE_DATA \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_INCLASS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPSGAME_API UClass* StaticClass<class ATpsGamePlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TpsGame_Source_TpsGame_Game_TpsGamePlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
