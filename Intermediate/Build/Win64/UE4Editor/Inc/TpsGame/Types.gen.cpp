// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TpsGame/FuncLib/Types.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTypes() {}
// Cross Module References
	TPSGAME_API UEnum* Z_Construct_UEnum_TpsGame_EMovementState();
	UPackage* Z_Construct_UPackage__Script_TpsGame();
	TPSGAME_API UScriptStruct* Z_Construct_UScriptStruct_FWeaponProperties();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	TPSGAME_API UClass* Z_Construct_UClass_AWeaponBase_NoRegister();
	TPSGAME_API UScriptStruct* Z_Construct_UScriptStruct_FWeaponDispersion();
	ENGINE_API UClass* Z_Construct_UClass_USoundBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	TPSGAME_API UScriptStruct* Z_Construct_UScriptStruct_FProjectileProperties();
	TPSGAME_API UClass* Z_Construct_UClass_AProjectileBase_NoRegister();
	TPSGAME_API UScriptStruct* Z_Construct_UScriptStruct_FMovementSpeed();
	TPSGAME_API UClass* Z_Construct_UClass_UTypes_NoRegister();
	TPSGAME_API UClass* Z_Construct_UClass_UTypes();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
	static UEnum* EMovementState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TpsGame_EMovementState, Z_Construct_UPackage__Script_TpsGame(), TEXT("EMovementState"));
		}
		return Singleton;
	}
	template<> TPSGAME_API UEnum* StaticEnum<EMovementState>()
	{
		return EMovementState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMovementState(EMovementState_StaticEnum, TEXT("/Script/TpsGame"), TEXT("EMovementState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TpsGame_EMovementState_Hash() { return 1473912741U; }
	UEnum* Z_Construct_UEnum_TpsGame_EMovementState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TpsGame();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMovementState"), 0, Get_Z_Construct_UEnum_TpsGame_EMovementState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMovementState::Walk_State", (int64)EMovementState::Walk_State },
				{ "EMovementState::Sprint_State", (int64)EMovementState::Sprint_State },
				{ "EMovementState::Aim_State", (int64)EMovementState::Aim_State },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Aim_State.DisplayName", "Aim" },
				{ "Aim_State.Name", "EMovementState::Aim_State" },
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "FuncLib/Types.h" },
				{ "Sprint_State.DisplayName", "Sprint" },
				{ "Sprint_State.Name", "EMovementState::Sprint_State" },
				{ "Walk_State.DisplayName", "Walk" },
				{ "Walk_State.Name", "EMovementState::Walk_State" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TpsGame,
				nullptr,
				"EMovementState",
				"EMovementState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FWeaponProperties>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FWeaponProperties cannot be polymorphic unless super FTableRowBase is polymorphic");

class UScriptStruct* FWeaponProperties::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TPSGAME_API uint32 Get_Z_Construct_UScriptStruct_FWeaponProperties_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWeaponProperties, Z_Construct_UPackage__Script_TpsGame(), TEXT("WeaponProperties"), sizeof(FWeaponProperties), Get_Z_Construct_UScriptStruct_FWeaponProperties_Hash());
	}
	return Singleton;
}
template<> TPSGAME_API UScriptStruct* StaticStruct<FWeaponProperties>()
{
	return FWeaponProperties::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWeaponProperties(FWeaponProperties::StaticStruct, TEXT("/Script/TpsGame"), TEXT("WeaponProperties"), false, nullptr, nullptr);
static struct FScriptStruct_TpsGame_StaticRegisterNativesFWeaponProperties
{
	FScriptStruct_TpsGame_StaticRegisterNativesFWeaponProperties()
	{
		UScriptStruct::DeferCppStructOps<FWeaponProperties>(FName(TEXT("WeaponProperties")));
	}
} ScriptStruct_TpsGame_StaticRegisterNativesFWeaponProperties;
	struct Z_Construct_UScriptStruct_FWeaponProperties_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WeaponClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FireRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReloadTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReloadTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxAmmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxAmmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Dispersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Dispersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FireSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReloadSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReloadSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireEffect_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FireEffect;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectileSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectileSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceDamage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TraceDamage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TraceDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWeaponProperties>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_WeaponClass_MetaData[] = {
		{ "Category", "Class" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_WeaponClass = { "WeaponClass", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, WeaponClass), Z_Construct_UClass_AWeaponBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_WeaponClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_WeaponClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireRate_MetaData[] = {
		{ "Category", "Stats" },
		{ "Comment", "/**The fire rate of the weapon (rounds/second)*/" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
		{ "ToolTip", "The fire rate of the weapon (rounds/second)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireRate = { "FireRate", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, FireRate), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadTime_MetaData[] = {
		{ "Category", "Stats" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadTime = { "ReloadTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, ReloadTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_MaxAmmo_MetaData[] = {
		{ "Category", "Stats" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_MaxAmmo = { "MaxAmmo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, MaxAmmo), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_MaxAmmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_MaxAmmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_Dispersion_MetaData[] = {
		{ "Category", "Dispersion" },
		{ "Comment", "//\n" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_Dispersion = { "Dispersion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, Dispersion), Z_Construct_UScriptStruct_FWeaponDispersion, METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_Dispersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_Dispersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "//\n" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireSound = { "FireSound", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, FireSound), Z_Construct_UClass_USoundBase_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireSound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadSound = { "ReloadSound", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, ReloadSound), Z_Construct_UClass_USoundBase_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadSound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireEffect_MetaData[] = {
		{ "Category", "Particle" },
		{ "Comment", "//\n" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireEffect = { "FireEffect", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, FireEffect), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireEffect_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireEffect_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ProjectileSettings_MetaData[] = {
		{ "Category", "Projectile" },
		{ "Comment", "//\n" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ProjectileSettings = { "ProjectileSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, ProjectileSettings), Z_Construct_UScriptStruct_FProjectileProperties, METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ProjectileSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ProjectileSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDamage_MetaData[] = {
		{ "Category", "Trace" },
		{ "Comment", "//\n" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDamage = { "TraceDamage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, TraceDamage), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDamage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDamage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDistance_MetaData[] = {
		{ "Category", "Trace" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDistance = { "TraceDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponProperties, TraceDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWeaponProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_WeaponClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_MaxAmmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_Dispersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ReloadSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_FireEffect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_ProjectileSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDamage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponProperties_Statics::NewProp_TraceDistance,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWeaponProperties_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"WeaponProperties",
		sizeof(FWeaponProperties),
		alignof(FWeaponProperties),
		Z_Construct_UScriptStruct_FWeaponProperties_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponProperties_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponProperties_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWeaponProperties()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWeaponProperties_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TpsGame();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WeaponProperties"), sizeof(FWeaponProperties), Get_Z_Construct_UScriptStruct_FWeaponProperties_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWeaponProperties_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWeaponProperties_Hash() { return 91261941U; }
class UScriptStruct* FWeaponDispersion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TPSGAME_API uint32 Get_Z_Construct_UScriptStruct_FWeaponDispersion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWeaponDispersion, Z_Construct_UPackage__Script_TpsGame(), TEXT("WeaponDispersion"), sizeof(FWeaponDispersion), Get_Z_Construct_UScriptStruct_FWeaponDispersion_Hash());
	}
	return Singleton;
}
template<> TPSGAME_API UScriptStruct* StaticStruct<FWeaponDispersion>()
{
	return FWeaponDispersion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWeaponDispersion(FWeaponDispersion::StaticStruct, TEXT("/Script/TpsGame"), TEXT("WeaponDispersion"), false, nullptr, nullptr);
static struct FScriptStruct_TpsGame_StaticRegisterNativesFWeaponDispersion
{
	FScriptStruct_TpsGame_StaticRegisterNativesFWeaponDispersion()
	{
		UScriptStruct::DeferCppStructOps<FWeaponDispersion>(FName(TEXT("WeaponDispersion")));
	}
} ScriptStruct_TpsGame_StaticRegisterNativesFWeaponDispersion;
	struct Z_Construct_UScriptStruct_FWeaponDispersion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialDispersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InitialDispersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinDispersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinDispersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxDispersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxDispersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AimDispersionCoef_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AimDispersionCoef;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponDispersion_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWeaponDispersion>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_InitialDispersion_MetaData[] = {
		{ "Category", "Dispersion" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_InitialDispersion = { "InitialDispersion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponDispersion, InitialDispersion), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_InitialDispersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_InitialDispersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MinDispersion_MetaData[] = {
		{ "Category", "Dispersion" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MinDispersion = { "MinDispersion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponDispersion, MinDispersion), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MinDispersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MinDispersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MaxDispersion_MetaData[] = {
		{ "Category", "Dispersion" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MaxDispersion = { "MaxDispersion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponDispersion, MaxDispersion), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MaxDispersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MaxDispersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_AimDispersionCoef_MetaData[] = {
		{ "Category", "Dispersion" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_AimDispersionCoef = { "AimDispersionCoef", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponDispersion, AimDispersionCoef), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_AimDispersionCoef_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_AimDispersionCoef_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWeaponDispersion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_InitialDispersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MinDispersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_MaxDispersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponDispersion_Statics::NewProp_AimDispersionCoef,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWeaponDispersion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
		nullptr,
		&NewStructOps,
		"WeaponDispersion",
		sizeof(FWeaponDispersion),
		alignof(FWeaponDispersion),
		Z_Construct_UScriptStruct_FWeaponDispersion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponDispersion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWeaponDispersion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWeaponDispersion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TpsGame();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WeaponDispersion"), sizeof(FWeaponDispersion), Get_Z_Construct_UScriptStruct_FWeaponDispersion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWeaponDispersion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWeaponDispersion_Hash() { return 2990509557U; }
class UScriptStruct* FProjectileProperties::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TPSGAME_API uint32 Get_Z_Construct_UScriptStruct_FProjectileProperties_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FProjectileProperties, Z_Construct_UPackage__Script_TpsGame(), TEXT("ProjectileProperties"), sizeof(FProjectileProperties), Get_Z_Construct_UScriptStruct_FProjectileProperties_Hash());
	}
	return Singleton;
}
template<> TPSGAME_API UScriptStruct* StaticStruct<FProjectileProperties>()
{
	return FProjectileProperties::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FProjectileProperties(FProjectileProperties::StaticStruct, TEXT("/Script/TpsGame"), TEXT("ProjectileProperties"), false, nullptr, nullptr);
static struct FScriptStruct_TpsGame_StaticRegisterNativesFProjectileProperties
{
	FScriptStruct_TpsGame_StaticRegisterNativesFProjectileProperties()
	{
		UScriptStruct::DeferCppStructOps<FProjectileProperties>(FName(TEXT("ProjectileProperties")));
	}
} ScriptStruct_TpsGame_StaticRegisterNativesFProjectileProperties;
	struct Z_Construct_UScriptStruct_FProjectileProperties_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Projectile_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Projectile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Lifetime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Lifetime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Velocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Velocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanBounce_MetaData[];
#endif
		static void NewProp_CanBounce_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CanBounce;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxDmgRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxDmgRadius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FProjectileProperties_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FProjectileProperties>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Projectile_MetaData[] = {
		{ "Category", "Projectile Settings" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Projectile = { "Projectile", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FProjectileProperties, Projectile), Z_Construct_UClass_AProjectileBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Projectile_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Projectile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "Projectile Settings" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Damage = { "Damage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FProjectileProperties, Damage), METADATA_PARAMS(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Damage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Damage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Lifetime_MetaData[] = {
		{ "Category", "Projectile Settings" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Lifetime = { "Lifetime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FProjectileProperties, Lifetime), METADATA_PARAMS(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Lifetime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Lifetime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Velocity_MetaData[] = {
		{ "Category", "Projectile Settings" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Velocity = { "Velocity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FProjectileProperties, Velocity), METADATA_PARAMS(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Velocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Velocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_CanBounce_MetaData[] = {
		{ "Category", "Projectile Settings" },
		{ "Comment", "// To do: Hit Effect\n" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
		{ "ToolTip", "To do: Hit Effect" },
	};
#endif
	void Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_CanBounce_SetBit(void* Obj)
	{
		((FProjectileProperties*)Obj)->CanBounce = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_CanBounce = { "CanBounce", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FProjectileProperties), &Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_CanBounce_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_CanBounce_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_CanBounce_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_MaxDmgRadius_MetaData[] = {
		{ "Category", "Projectile Settings" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_MaxDmgRadius = { "MaxDmgRadius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FProjectileProperties, MaxDmgRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_MaxDmgRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_MaxDmgRadius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FProjectileProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Projectile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Lifetime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_Velocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_CanBounce,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FProjectileProperties_Statics::NewProp_MaxDmgRadius,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FProjectileProperties_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
		nullptr,
		&NewStructOps,
		"ProjectileProperties",
		sizeof(FProjectileProperties),
		alignof(FProjectileProperties),
		Z_Construct_UScriptStruct_FProjectileProperties_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FProjectileProperties_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FProjectileProperties_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FProjectileProperties()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FProjectileProperties_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TpsGame();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ProjectileProperties"), sizeof(FProjectileProperties), Get_Z_Construct_UScriptStruct_FProjectileProperties_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FProjectileProperties_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FProjectileProperties_Hash() { return 935304505U; }
class UScriptStruct* FMovementSpeed::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TPSGAME_API uint32 Get_Z_Construct_UScriptStruct_FMovementSpeed_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovementSpeed, Z_Construct_UPackage__Script_TpsGame(), TEXT("MovementSpeed"), sizeof(FMovementSpeed), Get_Z_Construct_UScriptStruct_FMovementSpeed_Hash());
	}
	return Singleton;
}
template<> TPSGAME_API UScriptStruct* StaticStruct<FMovementSpeed>()
{
	return FMovementSpeed::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovementSpeed(FMovementSpeed::StaticStruct, TEXT("/Script/TpsGame"), TEXT("MovementSpeed"), false, nullptr, nullptr);
static struct FScriptStruct_TpsGame_StaticRegisterNativesFMovementSpeed
{
	FScriptStruct_TpsGame_StaticRegisterNativesFMovementSpeed()
	{
		UScriptStruct::DeferCppStructOps<FMovementSpeed>(FName(TEXT("MovementSpeed")));
	}
} ScriptStruct_TpsGame_StaticRegisterNativesFMovementSpeed;
	struct Z_Construct_UScriptStruct_FMovementSpeed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Walk_Speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Walk_Speed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sprint_Speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Sprint_Speed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Aim_Speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Aim_Speed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovementSpeed_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovementSpeed>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Walk_Speed_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Walk_Speed = { "Walk_Speed", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovementSpeed, Walk_Speed), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Walk_Speed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Walk_Speed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Sprint_Speed_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Sprint_Speed = { "Sprint_Speed", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovementSpeed, Sprint_Speed), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Sprint_Speed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Sprint_Speed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Aim_Speed_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Aim_Speed = { "Aim_Speed", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovementSpeed, Aim_Speed), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Aim_Speed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Aim_Speed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovementSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Walk_Speed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Sprint_Speed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovementSpeed_Statics::NewProp_Aim_Speed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovementSpeed_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
		nullptr,
		&NewStructOps,
		"MovementSpeed",
		sizeof(FMovementSpeed),
		alignof(FMovementSpeed),
		Z_Construct_UScriptStruct_FMovementSpeed_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovementSpeed_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovementSpeed_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovementSpeed_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovementSpeed()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovementSpeed_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TpsGame();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovementSpeed"), sizeof(FMovementSpeed), Get_Z_Construct_UScriptStruct_FMovementSpeed_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovementSpeed_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovementSpeed_Hash() { return 87417039U; }
	void UTypes::StaticRegisterNativesUTypes()
	{
	}
	UClass* Z_Construct_UClass_UTypes_NoRegister()
	{
		return UTypes::StaticClass();
	}
	struct Z_Construct_UClass_UTypes_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTypes_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTypes_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FuncLib/Types.h" },
		{ "ModuleRelativePath", "FuncLib/Types.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTypes_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTypes>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTypes_Statics::ClassParams = {
		&UTypes::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTypes_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTypes_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTypes()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTypes_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTypes, 1494335801);
	template<> TPSGAME_API UClass* StaticClass<UTypes>()
	{
		return UTypes::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTypes(Z_Construct_UClass_UTypes, &UTypes::StaticClass, TEXT("/Script/TpsGame"), TEXT("UTypes"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTypes);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
