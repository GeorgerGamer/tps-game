// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPSGAME_WorldItemBase_generated_h
#error "WorldItemBase.generated.h already included, missing '#pragma once' in WorldItemBase.h"
#endif
#define TPSGAME_WorldItemBase_generated_h

#define TpsGame_Source_TpsGame_WorldItemBase_h_12_SPARSE_DATA
#define TpsGame_Source_TpsGame_WorldItemBase_h_12_RPC_WRAPPERS
#define TpsGame_Source_TpsGame_WorldItemBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TpsGame_Source_TpsGame_WorldItemBase_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWorldItemBase(); \
	friend struct Z_Construct_UClass_AWorldItemBase_Statics; \
public: \
	DECLARE_CLASS(AWorldItemBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(AWorldItemBase)


#define TpsGame_Source_TpsGame_WorldItemBase_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAWorldItemBase(); \
	friend struct Z_Construct_UClass_AWorldItemBase_Statics; \
public: \
	DECLARE_CLASS(AWorldItemBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(AWorldItemBase)


#define TpsGame_Source_TpsGame_WorldItemBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWorldItemBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWorldItemBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWorldItemBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWorldItemBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWorldItemBase(AWorldItemBase&&); \
	NO_API AWorldItemBase(const AWorldItemBase&); \
public:


#define TpsGame_Source_TpsGame_WorldItemBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWorldItemBase(AWorldItemBase&&); \
	NO_API AWorldItemBase(const AWorldItemBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWorldItemBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWorldItemBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWorldItemBase)


#define TpsGame_Source_TpsGame_WorldItemBase_h_12_PRIVATE_PROPERTY_OFFSET
#define TpsGame_Source_TpsGame_WorldItemBase_h_9_PROLOG
#define TpsGame_Source_TpsGame_WorldItemBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_SPARSE_DATA \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_RPC_WRAPPERS \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_INCLASS \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TpsGame_Source_TpsGame_WorldItemBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_SPARSE_DATA \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_INCLASS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_WorldItemBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPSGAME_API UClass* StaticClass<class AWorldItemBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TpsGame_Source_TpsGame_WorldItemBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
