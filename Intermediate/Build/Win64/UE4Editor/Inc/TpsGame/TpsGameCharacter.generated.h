// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AWeaponBase;
enum class EMovementState : uint8;
#ifdef TPSGAME_TpsGameCharacter_generated_h
#error "TpsGameCharacter.generated.h already included, missing '#pragma once' in TpsGameCharacter.h"
#endif
#define TPSGAME_TpsGameCharacter_generated_h

#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_SPARSE_DATA
#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInputFireReleased); \
	DECLARE_FUNCTION(execInputFirePressed); \
	DECLARE_FUNCTION(execEventFire); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execSetMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX);


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInputFireReleased); \
	DECLARE_FUNCTION(execInputFirePressed); \
	DECLARE_FUNCTION(execEventFire); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execSetMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX);


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATpsGameCharacter(); \
	friend struct Z_Construct_UClass_ATpsGameCharacter_Statics; \
public: \
	DECLARE_CLASS(ATpsGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(ATpsGameCharacter)


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATpsGameCharacter(); \
	friend struct Z_Construct_UClass_ATpsGameCharacter_Statics; \
public: \
	DECLARE_CLASS(ATpsGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(ATpsGameCharacter)


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATpsGameCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATpsGameCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATpsGameCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATpsGameCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATpsGameCharacter(ATpsGameCharacter&&); \
	NO_API ATpsGameCharacter(const ATpsGameCharacter&); \
public:


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATpsGameCharacter(ATpsGameCharacter&&); \
	NO_API ATpsGameCharacter(const ATpsGameCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATpsGameCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATpsGameCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATpsGameCharacter)


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ATpsGameCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATpsGameCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(ATpsGameCharacter, CursorToWorld); }


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_11_PROLOG
#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_SPARSE_DATA \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_RPC_WRAPPERS \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_INCLASS \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_SPARSE_DATA \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_INCLASS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_Character_TpsGameCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPSGAME_API UClass* StaticClass<class ATpsGameCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TpsGame_Source_TpsGame_Character_TpsGameCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
