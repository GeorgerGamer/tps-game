// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPSGAME_WeaponBase_generated_h
#error "WeaponBase.generated.h already included, missing '#pragma once' in WeaponBase.h"
#endif
#define TPSGAME_WeaponBase_generated_h

#define TpsGame_Source_TpsGame_WeaponBase_h_15_SPARSE_DATA
#define TpsGame_Source_TpsGame_WeaponBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetIsFiring);


#define TpsGame_Source_TpsGame_WeaponBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetIsFiring);


#define TpsGame_Source_TpsGame_WeaponBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeaponBase(); \
	friend struct Z_Construct_UClass_AWeaponBase_Statics; \
public: \
	DECLARE_CLASS(AWeaponBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBase)


#define TpsGame_Source_TpsGame_WeaponBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAWeaponBase(); \
	friend struct Z_Construct_UClass_AWeaponBase_Statics; \
public: \
	DECLARE_CLASS(AWeaponBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBase)


#define TpsGame_Source_TpsGame_WeaponBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeaponBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBase(AWeaponBase&&); \
	NO_API AWeaponBase(const AWeaponBase&); \
public:


#define TpsGame_Source_TpsGame_WeaponBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBase(AWeaponBase&&); \
	NO_API AWeaponBase(const AWeaponBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeaponBase)


#define TpsGame_Source_TpsGame_WeaponBase_h_15_PRIVATE_PROPERTY_OFFSET
#define TpsGame_Source_TpsGame_WeaponBase_h_12_PROLOG
#define TpsGame_Source_TpsGame_WeaponBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_WeaponBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_WeaponBase_h_15_SPARSE_DATA \
	TpsGame_Source_TpsGame_WeaponBase_h_15_RPC_WRAPPERS \
	TpsGame_Source_TpsGame_WeaponBase_h_15_INCLASS \
	TpsGame_Source_TpsGame_WeaponBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TpsGame_Source_TpsGame_WeaponBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_WeaponBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_WeaponBase_h_15_SPARSE_DATA \
	TpsGame_Source_TpsGame_WeaponBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_WeaponBase_h_15_INCLASS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_WeaponBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPSGAME_API UClass* StaticClass<class AWeaponBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TpsGame_Source_TpsGame_WeaponBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
