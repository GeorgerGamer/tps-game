// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TpsGame/WorldItemBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWorldItemBase() {}
// Cross Module References
	TPSGAME_API UClass* Z_Construct_UClass_AWorldItemBase_NoRegister();
	TPSGAME_API UClass* Z_Construct_UClass_AWorldItemBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_TpsGame();
// End Cross Module References
	void AWorldItemBase::StaticRegisterNativesAWorldItemBase()
	{
	}
	UClass* Z_Construct_UClass_AWorldItemBase_NoRegister()
	{
		return AWorldItemBase::StaticClass();
	}
	struct Z_Construct_UClass_AWorldItemBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWorldItemBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TpsGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldItemBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WorldItemBase.h" },
		{ "ModuleRelativePath", "WorldItemBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWorldItemBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWorldItemBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWorldItemBase_Statics::ClassParams = {
		&AWorldItemBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWorldItemBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldItemBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWorldItemBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWorldItemBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWorldItemBase, 1053066485);
	template<> TPSGAME_API UClass* StaticClass<AWorldItemBase>()
	{
		return AWorldItemBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWorldItemBase(Z_Construct_UClass_AWorldItemBase, &AWorldItemBase::StaticClass, TEXT("/Script/TpsGame"), TEXT("AWorldItemBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWorldItemBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
