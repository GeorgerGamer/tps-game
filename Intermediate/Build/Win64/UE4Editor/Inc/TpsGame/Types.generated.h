// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPSGAME_Types_generated_h
#error "Types.generated.h already included, missing '#pragma once' in Types.h"
#endif
#define TPSGAME_Types_generated_h

#define TpsGame_Source_TpsGame_FuncLib_Types_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponProperties_Statics; \
	TPSGAME_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> TPSGAME_API UScriptStruct* StaticStruct<struct FWeaponProperties>();

#define TpsGame_Source_TpsGame_FuncLib_Types_h_56_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponDispersion_Statics; \
	TPSGAME_API static class UScriptStruct* StaticStruct();


template<> TPSGAME_API UScriptStruct* StaticStruct<struct FWeaponDispersion>();

#define TpsGame_Source_TpsGame_FuncLib_Types_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FProjectileProperties_Statics; \
	TPSGAME_API static class UScriptStruct* StaticStruct();


template<> TPSGAME_API UScriptStruct* StaticStruct<struct FProjectileProperties>();

#define TpsGame_Source_TpsGame_FuncLib_Types_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovementSpeed_Statics; \
	TPSGAME_API static class UScriptStruct* StaticStruct();


template<> TPSGAME_API UScriptStruct* StaticStruct<struct FMovementSpeed>();

#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_SPARSE_DATA
#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_RPC_WRAPPERS
#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_RPC_WRAPPERS_NO_PURE_DECLS
#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_INCLASS \
private: \
	static void StaticRegisterNativesUTypes(); \
	friend struct Z_Construct_UClass_UTypes_Statics; \
public: \
	DECLARE_CLASS(UTypes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TpsGame"), NO_API) \
	DECLARE_SERIALIZER(UTypes)


#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public:


#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTypes(UTypes&&); \
	NO_API UTypes(const UTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTypes)


#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_PRIVATE_PROPERTY_OFFSET
#define TpsGame_Source_TpsGame_FuncLib_Types_h_117_PROLOG
#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_SPARSE_DATA \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_RPC_WRAPPERS \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_INCLASS \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TpsGame_Source_TpsGame_FuncLib_Types_h_120_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_PRIVATE_PROPERTY_OFFSET \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_SPARSE_DATA \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_RPC_WRAPPERS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_INCLASS_NO_PURE_DECLS \
	TpsGame_Source_TpsGame_FuncLib_Types_h_120_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPSGAME_API UClass* StaticClass<class UTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TpsGame_Source_TpsGame_FuncLib_Types_h


#define FOREACH_ENUM_EMOVEMENTSTATE(op) \
	op(EMovementState::Walk_State) \
	op(EMovementState::Sprint_State) \
	op(EMovementState::Aim_State) 

enum class EMovementState : uint8;
template<> TPSGAME_API UEnum* StaticEnum<EMovementState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
