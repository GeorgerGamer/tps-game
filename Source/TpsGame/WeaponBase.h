// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "FuncLib/Types.h"
#include <GameFramework/CharacterMovementComponent.h>
#include "WeaponBase.generated.h"

UCLASS()
class TPSGAME_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* Scene = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SK_WeaponMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* SM_WeaponMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* MuzzleArrow = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		FWeaponProperties WeaponSettings;

	EMovementState OwnerMovementState;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
		bool IsFiring = false;

	float FireTime = 0;

	void FireTick(float DeltaTime);

	void WeaponInit();

	FProjectileProperties GetProjectile();

	UFUNCTION()
		void SetIsFiring(bool bFiring);

	void Fire();
};
