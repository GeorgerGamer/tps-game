// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Walk_State UMETA(DisplayName = "Walk"),
	Sprint_State UMETA(DisplayName = "Sprint"),
	Aim_State UMETA(DisplayName = "Aim")
};

USTRUCT(BlueprintType)
struct FMovementSpeed
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		float Walk_Speed = 300.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		float Sprint_Speed = 600.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		float Aim_Speed = 180.0f;
};

USTRUCT(BlueprintType)
struct FProjectileProperties
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		TSubclassOf<class AProjectileBase> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		float Damage = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		TSubclassOf<UDamageType> NormalDamageType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		float Lifetime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		float Velocity = 3000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		int32 PiercingLvl = 0;

	// To do: Hit Effect

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		bool IsExplosive;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		float MaxExplDmg = 150.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		TSubclassOf<UDamageType> ExplosiveDamageType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		float MaxExplRad = 500.0f;

};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float InitialDispersion = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float MinDispersion = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float MaxDispersion = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimDispersionCoef = 1.0f;
};

USTRUCT(BlueprintType)
struct FWeaponProperties : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class AWeaponBase> WeaponClass = nullptr;

	//

	/**The fire rate of the weapon (rounds/second)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float FireRate = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float ReloadTime = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		int32 MaxAmmo = 15;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion Dispersion;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* FireSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* ReloadSound = nullptr;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Particle")
		UParticleSystem* FireEffect = nullptr;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileProperties ProjectileSettings;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float TraceDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float TraceDistance = 500.0f;

};

UCLASS()
class TPSGAME_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};
