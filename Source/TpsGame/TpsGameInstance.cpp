// Fill out your copyright notice in the Description page of Project Settings.


#include "TpsGameInstance.h"

bool UTpsGameInstance::GetWeaponByName(FName WeaponName, FWeaponProperties& OutputWeapon)
{
	bool bFound = false;

	FWeaponProperties* NewWeapon = WeaponTable->FindRow<FWeaponProperties>(WeaponName, "", false);
	if (NewWeapon)
	{
		bFound = true;
		OutputWeapon = *NewWeapon;

	}
	return bFound;

}

