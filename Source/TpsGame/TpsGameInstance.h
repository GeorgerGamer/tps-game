// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "FuncLib/Types.h"
#include "TpsGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TPSGAME_API UTpsGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	UDataTable* WeaponTable = nullptr;

	UFUNCTION(BlueprintCallable)
	bool GetWeaponByName(FName WeaponName, FWeaponProperties& OutputWeapon);
	
};
