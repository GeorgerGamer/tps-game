// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "ProjectileBase.h"
#include "Character/TpsGameCharacter.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>("Scene");
	RootComponent = Scene;

	SK_WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("Skeletal Weapon Mesh");
	SK_WeaponMesh->SetGenerateOverlapEvents(false);
	SK_WeaponMesh->SetCollisionProfileName(TEXT("No Collision"));
	SK_WeaponMesh->SetupAttachment(Scene);

	SM_WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Weapon Mesh");
	SM_WeaponMesh->SetGenerateOverlapEvents(false);
	SM_WeaponMesh->SetCollisionProfileName(TEXT("No Collision"));
	SM_WeaponMesh->SetupAttachment(Scene);

	MuzzleArrow = CreateDefaultSubobject<UArrowComponent>("Muzzle Arrow");
	MuzzleArrow->SetupAttachment(Scene);

}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::FireTick(float DeltaTime)
{
	if (FireTime <= 0.0f)
	{
		if (IsFiring)
		{
			Fire();

		}
	}
	else
	{
		FireTime -= DeltaTime;

	}

}

void AWeaponBase::WeaponInit()
{
	if (SK_WeaponMesh && SK_WeaponMesh->SkeletalMesh)
	{
		SK_WeaponMesh->DestroyComponent(true);

	}

	if (SM_WeaponMesh && SM_WeaponMesh->GetStaticMesh())
	{
		SK_WeaponMesh->DestroyComponent();

	}

}

FProjectileProperties AWeaponBase::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;

}

void AWeaponBase::SetIsFiring(bool bFiring)
{
	if (OwnerMovementState != EMovementState::Sprint_State)
	{
		IsFiring = bFiring;

	}
	else
	{
		IsFiring = false;

	}

}

void AWeaponBase::Fire()
{
	FireTime = 1 / WeaponSettings.FireRate;

	if (MuzzleArrow)
	{
		FVector SpawnLocation = MuzzleArrow->GetComponentLocation();
		FRotator SpawnRotation = MuzzleArrow->GetComponentRotation();
		FProjectileProperties ProjectileProperties;
		ProjectileProperties = GetProjectile();

		if (ProjectileProperties.Projectile)
		{
			FActorSpawnParameters SpawnParam;
			SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParam.Owner = GetOwner();
			SpawnParam.Instigator = GetInstigator();

			AProjectileBase* DisBullet = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileProperties.Projectile, &SpawnLocation, &SpawnRotation, SpawnParam));

			if (DisBullet)
			{
				DisBullet->ProjectileSettings = ProjectileProperties;
				DisBullet->InitialLifeSpan = ProjectileProperties.Lifetime;

			}

		}

	}

}

