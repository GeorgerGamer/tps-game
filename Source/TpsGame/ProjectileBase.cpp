// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBase.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	CollisionSphere->SetSphereRadius(15.0f);

	CollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileBase::CollisionSphereHit);
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileBase::CollisionSphereBeginOverlap);
	CollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileBase::CollisionSphereEndOverlap);

	CollisionSphere->bReturnMaterialOnMove = true;
	CollisionSphere->SetCanEverAffectNavigation(false);

	RootComponent = CollisionSphere;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileMesh->SetCanEverAffectNavigation(false);

	TraceParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile Particle"));
	TraceParticle->SetupAttachment(RootComponent);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovement->UpdatedComponent = RootComponent;
	ProjectileMovement->InitialSpeed = ProjectileSettings.Velocity;
	ProjectileMovement->MaxSpeed = 0;

	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileBase::ExplosiveTick()
{
	if (ProjectileSettings.IsExplosive)
	{


	}

}

void AProjectileBase::Detonate()
{


}

void AProjectileBase::CollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector ImpactNormal, const FHitResult& HitRes)
{
	if (ProjectileSettings.IsExplosive)
	{
		
	
	}
	else
	{
		Destroy();

	}
}

void AProjectileBase::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& HitRes)
{
	for (int32 i = 0; i < ProjectileSettings.PiercingLvl; i++)
	{
		UGameplayStatics::ApplyDamage(OtherActor, ProjectileSettings.Damage, {}, this, ProjectileSettings.NormalDamageType);
	}

}

void AProjectileBase::CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{


}

