// Copyright Epic Games, Inc. All Rights Reserved.

#include "TpsGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TpsGame, "TpsGame" );

DEFINE_LOG_CATEGORY(LogTpsGame)
 