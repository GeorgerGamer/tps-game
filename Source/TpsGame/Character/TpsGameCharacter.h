// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLib/Types.h"
#include "../WeaponBase.h"
#include "TpsGameCharacter.generated.h"

UCLASS(Blueprintable)
class ATpsGameCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATpsGameCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComp) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* CursorToWorld;

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement");
		FMovementSpeed MovementStats;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		FName StartingWeaponName;


	AWeaponBase* CurrentWeapon = nullptr;

	//

	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);

	float AxisX, AxisY = 0.0f;

	void MovementTick(float DeltaTime);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		bool IsStunned = false;

	//

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void SetMovementState(EMovementState NewState);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Vitals")
		float Stamina{};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float MaxStamina{};

	//

	UFUNCTION(BlueprintCallable)
		AWeaponBase* GetCurrentWeapon();


	void InitWeapon(FName WeaponID);

	//

	UFUNCTION(BlueprintCallable)
		void EventFire(bool bFiring);

	UFUNCTION()
		void InputFirePressed();
	UFUNCTION()
		void InputFireReleased();

};
